package com.gitee.jastee.kafka.constant;

import org.apache.kafka.clients.producer.ProducerConfig;

/**
 * @author Jast
 * @description
 * @date 2022-03-03 13:28
 */
public class KafkaConstants {

    /** 配置文件中kafka前缀 */
    public static final String KAFKA_PREFIX = "kafka.";

    /** Topic */
    public static final String KAFKA_TOPIC = KAFKA_PREFIX + "topic";

    public static final String KAFKA_GROUP_ID = KAFKA_PREFIX + "group.id";

    /** 消费Kafka休眠时间 */
    public static final String KAFKA_SLEEP_TIME = KAFKA_PREFIX + "consumer.sleep";

    public static final String DATA = KAFKA_PREFIX + "data";

    /** Kafka地址 */
    public static final String KAFKA_BOOTSTRAP_SERVERS_CONFIG =
            KAFKA_PREFIX + ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;

    /** 删除offset的分区 */
    public static final String DELETE_OFFSET_PARTITION = KAFKA_PREFIX + "delete.partition";

    /** 删除offset之前的数据 */
    public static final String DELETE_OFFSET = KAFKA_PREFIX + "delete.offset";

    /** 分区数量 */
    public static final String KAFKA_NUM_PARTITIONS = KAFKA_PREFIX + "partition.num";

    /** 副本数量 */
    public static final String KAFKA_NUM_REPLICATION_FACTOR = KAFKA_PREFIX + "replication.factor";

    /** 创建Topic数据过期时间 */
    public static final String KAFKA_RETENTION_MS = KAFKA_PREFIX + "retention.ms";
}
