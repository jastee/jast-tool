package com.gitee.jastee.kafka.tool;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

public class KafkaProducerToolTest {
    private String kafkaBootstrap = "10.16.0.2:9092";

    @Test
    public void send() throws ExecutionException, InterruptedException {
        String[] strings =
                new String[] {
                    "--kafka.bootstrap.servers",
                    kafkaBootstrap,
                    "--kafka.topic",
                    "test20230103_7",
                    "--kafka.data",
                    "{\"code\":\"sdfasdf3\",\"name\":\"ID4车主\",\"one_id\":\"-1eko87URIFaniIm6AF3m\",\"source\":1,\"toTime\":\"20220523\",\"type\":3}"
                };
        KafkaProducerTool.send(strings);
    }
}
