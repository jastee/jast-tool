package com.gitee.jastee.kafka.tool;

import cn.hutool.core.lang.Tuple;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

public class KafkaAdminToolTest {

    // private String kafkaBootstrap = "0.0.0.0:9092";
    private String kafkaBootstrap = "10.16.0.2:9092";

    @Test
    public void createTopic() throws ExecutionException, InterruptedException {
        String[] strings =
                new String[] {
                    "--kafka.bootstrap.servers",
                    kafkaBootstrap,
                    "--kafka.topic",
                    "test20230103_7",
                    "--kafka.retention.ms",
                    "6000",
                    "--kafka.replication.factor",
                    "1",
                    "--kafka.partition.num",
                    "1"
                };

        KafkaAdminTool.createTopic(strings);
    }

    @Test
    public void describeTopic() throws ExecutionException, InterruptedException {
        String[] strings =
                new String[] {
                    "--kafka.bootstrap.servers", kafkaBootstrap, "--kafka.topic", "test20230103_6"
                };

        Tuple describeTopic = KafkaAdminTool.describeTopic(strings);
        System.out.println(describeTopic);
    }

    @Test
    public void deleteTopic() throws ExecutionException, InterruptedException {
        String[] strings =
                new String[] {
                    "--kafka.bootstrap.servers", kafkaBootstrap, "--kafka.topic", "test20230103_7"
                };
        KafkaAdminTool.deleteTopic(strings);
    }
}
