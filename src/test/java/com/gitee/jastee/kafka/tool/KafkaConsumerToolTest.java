package com.gitee.jastee.kafka.tool;

import org.junit.Test;

public class KafkaConsumerToolTest {
    private String kafkaBootstrap = "10.16.0.2:9092";

    @Test
    public void consumer() throws InterruptedException {
        String[] strings =
                new String[] {
                    "--kafka.bootstrap.servers", kafkaBootstrap, "--kafka.topic", "boEvents"
                };

        KafkaConsumerTool.consumer(strings);
    }
}
