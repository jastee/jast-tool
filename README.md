## 日志配置
### 输出DEBUG日志
在调用项目中resources目录下创建log4j.properties
```shell script
# 设置日志级别
log4j.rootLogger=debug,STDOUT

log4j.additivity.org.apache=true
log4j.appender.STDOUT=org.apache.log4j.ConsoleAppender
log4j.appender.STDOUT.layout=org.apache.log4j.PatternLayout
log4j.appender.STDOUT.layout.ConversionPattern=[%d{HH:mm:ss,SSS}][%5p] %c:%L - %m%n
```
## 查询ElasticSearch
### 根据SQL查询ElasticSearch数据
- 调用方法
  类：com.gitee.jastee.es.ElasticSearchSQL
  方法：public static String query(String[] args)

  代码调用：

  ```java
  String query = com.gitee.jastee.es.ElasticSearchSQL.query(args);
  System.out.println(query);
  ```

- 传入参数
样例:
```shell script
--es.address 10.16.0.33:9200 --es.sql "select * from ads_user_profile limit 1 " --es.security true --es.authorization "Basic ZWxhc3RpYzpBdnJpczJAMjIjIQ==" --format true
```
或

```shell
--es.address 10.16.0.33:9200 --es.sql "select * from ads_user_profile limit 1 " --es.security true --es.username elastic --es.password password123 --format true
```



参数说明：

|参数|说明|默认值|样例|
|---|---|---|---|
|--es.address|ES地址|-|10.16.0.33:9200|
|--es.sql|查询SQL|-|select * from ads_user_profile|
|--es.security|是否启用认证|false|true|
|--es.authorization|认证配置|-|Basic ZWxhc3RpYzpBdnJpczJAMjIjIQ==|
|--es.username|认证用户(与es.authorization二选一)|-|elastic|
|--es.password|认证密码(与es.authorization二选一)|-|elasticpassword|
|--format|输出数据是否格式化打印|false|true|

## Kafka工具
### 删除指定Partition和offset之前的数据
|参数|说明|默认值|样例|
|---|---|---|---|
|--kafka.topic|Kafka Topic|-|topicName|
|--kafka.bootstrap.servers|连接Kafka地址|0.0.0.0:9092|10.8.10.10:9092|
|--kafka.delete.partition|需要删除的分区|-|0|
|--kafka.delete.offset|需要删除的Offset|-|0|
